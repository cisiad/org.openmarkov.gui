/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.core.gui.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;

import org.junit.Test;
import org.openmarkov.core.model.network.NodeType;


/**
 * This class tests the class {@link openmarkov.gui.networks.NetworkType}.
 * 
 * @author jmendoza
 */
public class NetworkTypeTest {
	/**
	 * This method tests the type of nodes that can be inserted into each type
	 * of network.
	 */
	@Test
	public final void testGetNodeTypes() {
		HashSet<NodeType> nodeTypes;

		nodeTypes = NetworkType.BAYESIAN_NET.getNodeTypes();
		assertEquals(nodeTypes.size(), 1);
		assertTrue(nodeTypes.contains(NodeType.CHANCE));
		nodeTypes = NetworkType.INFLUENCE_DIAGRAM.getNodeTypes();
		assertEquals(nodeTypes.size(), 3);
		assertTrue(nodeTypes.contains(NodeType.CHANCE));
		assertTrue(nodeTypes.contains(NodeType.DECISION));
		assertTrue(nodeTypes.contains(NodeType.UTILITY));
		nodeTypes = NetworkType.MARKOV_NET.getNodeTypes();
		assertEquals(nodeTypes.size(), 0);
		nodeTypes = NetworkType.CHAIN_GRAPH.getNodeTypes();
		assertEquals(nodeTypes.size(), 0);
	}
}
